import Vue from 'vue'
import App from './App.vue'

import ElementUI from 'element-ui'            //引入ElementUI
import 'element-ui/lib/theme-chalk/index.css' //导入样式
Vue.use(ElementUI)                            //使用

import router from './router'                 //引入router

// import axios from 'axios'                     //引入axios
// Vue.prototype.axios = axios                   // 挂载到原型上，可以全局使用
import service from './service'
Vue.prototype.service = service  // 挂载到原型上，可以全局使用

Vue.config.productionTip = false

new Vue({
  router,   //渲染之前挂载一下
  render: h => h(App),
}).$mount('#app')
