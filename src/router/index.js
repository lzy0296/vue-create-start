import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)
export default new Router({
    routes:[
        {
            path:'/',
            redirect:'/login',
            component:()=> import('@/components/Login') //路由懒加载
        },
        {
            path:'/login',
            name:'/Login',
            component:()=> import('@/components/Login') //路由懒加载
        },
        {
            path:'*',
            name:'NotFound',
            hidden:true,
            component:()=> import('@/components/NotFound') //路由懒加载
        },
        // {
        //     path:'/home',
        //     name:'home',
        //     // component:()=> import('@/components/Home.vue') //路由懒加载
        //     component:resolve => require(['@/components/Home'],resolve)  //异步组件
        // }
        // 第一个
        {
            path:'/home',
            name:'库存管理',
            redirect:'/home/bike',  //默认重定向
            component:()=> import('@/components/Home'),
            children:[
                {
                    path:'/home/bike',  
                    name:'商品信息',
                    component:()=> import('@/components/bike/BikeList'),//这里对应文件的名字，我创建的是BikeList
                },
                {
                    path:'/home/ment',  
                    name:'库存信息',
                    component:()=> import('@/components/bike/BikeMent'),//这里对应文件的名字，我创建的是BikeMent
                },
                {
                    path:'/home/info',  
                    name:'库存记录',
                    component:()=> import('@/components/bike/InfoList'),//这里对应文件的名字，我创建的是BikeList
                }
            ]
        },
 
        // 第二个
        {
            path:'/home',
            name:'数据渲染',
            component:()=> import('@/components/Home'),
            children:[
                {
                    path:'/home/dataview',  
                    name:'数据概览',
                    component:()=> import('@/components/bikeUpdate/DataView'),//这里对应文件的名字，我创建的是BikeList
                }
            ]
        }
    ],
    mode:'history'
})