// 项目中我们大多数时候都会把对应的接口请求都封装成api来调用
import service from '../service.js'
 
// 登录接口封装
export function login(data){
    return service({
        method: 'post',
        url:'/login',
        data
    })
}