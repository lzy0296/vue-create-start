import service from '../service.js'

// 商品信息接口封装
export function getPage(params){
    return service({
        method: 'get',
        url:'/Page',
        params
    })
}